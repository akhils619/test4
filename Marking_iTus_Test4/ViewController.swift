//
//  ViewController.swift
//  Marking_iTus_Test4
//
//  Created by Akhil S Raj on 2019-11-07.
//  Copyright © 2019 Akhil S Raj. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {
    @IBOutlet weak var timeElapsedLabel: UILabel!
    var USERNAME = "akhils619@gmail.com"
    var PASSWORD = "Flyhigh@619"
    var DEVICE_ID = "440035001047363333343437"
    
    var myPhoton: ParticleDevice!
    var timer = Timer()
    var time = 1
    var sliderReductionRate = 0
    var reduceValue = 0
    
    @IBOutlet weak var sliderValue: UILabel!
    @IBOutlet weak var amountOfChange: UISlider!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ParticleCloud.init()
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                print(error?.localizedDescription as Any)
            }
            else {
                print("Login success!")
                self.loadDeviceFromCloud()
            }
        }
        
    }
    
    func loadDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription as Any)
                return
            }
            else {
                print("Got photon from cloud: \(String(describing: device?.id))")
                self.myPhoton = device
                self.subscribeToParticleEvents()
            }
        }
    }
    
    func subscribeToParticleEvents() {
        print("Inside subscribe method")
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "display",
            deviceID:self.DEVICE_ID,
            handler: {
                (event :ParticleEvent?, error : Error?) in
                
                if let _ = error {
                    print("could not subscribe to events")
                } else {
                    print("got event with data \(event?.data)")
                    let userControl = (event?.data)!
                    print("The usercontrol received from particle is \(userControl)")
                }
        })
    }
    
    @IBAction func startMonitoring(_ sender: Any) {
        self.time = 1
        //self.sliderReductionRate = 0
        self.reduceValue = 0
        runTimer()
    }
    
    
    @IBAction func amountOfTImeChanged(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        self.sliderValue.text = "\(currentValue)"
        self.sliderReductionRate = currentValue
        
    }
    
    func runTimer() {
        print("Inside run timer")
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(ViewController.updateTimeElapsed)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimeElapsed(){
        
        if self.time >= 21 {
            self.timer.invalidate()
            self.time = 0
        }else{
            self.timeElapsedLabel.text = String(self.time)
            self.sendTimeToParticle()
        }
        
        if sliderReductionRate == 0 {
            self.time = time + 1
        }else {
            reduceValue = reduceValue + 1
            if reduceValue != self.sliderReductionRate {
                self.time = time + abs(reduceValue/sliderReductionRate)
            }else {
                self.time = time + 1
                reduceValue = 0
            }
        }
    }
    
    
    func sendTimeToParticle(){
        print("Invoked sendTimeToPArticle method with time \(self.time)")
        let parameters = [self.time]
        _ = myPhoton!.callFunction("displaySmile", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle ")
            }
            else {
                print("Error when telling Particle")
            }
        }
    }
    
}

